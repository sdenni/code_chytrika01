/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

import java.sql.ResultSet;

/**
 *
 * @author S_Denni
 */
public interface Master {
    public void Create();
    public void Save();
    public void Delete(String id);
    public ResultSet Show();
    public ResultSet Show(String id);
}
