/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

import java.sql.Date;
import java.sql.ResultSet;

/**
 *
 * @author S_Denni
 */
public interface Sejarah {
    public void Create();
    public ResultSet Show();
    public ResultSet ShowGrouped();
    public ResultSet ShowByGroup(String idGroup);
    public ResultSet Show(Date date);
    public ResultSet Show(Date date1, Date date2);
}
