/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.autocode;

import code.Counting;
import code.localsetting.AutoCode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class JenisAutoCode {
    private Counting counting = new Counting();
    
    String key = "J";
    
    public String UniqueCode(boolean reset){
        String value = "001";
        int count = 0;
        
        ResultSet res = counting.GetCounting();
        try {
            if(res.first()){
                if(reset){
                    count = 1;
                } else {
                    count = res.getInt("jenis_count") + 1;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(AutoCode.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(count < 10) {
            value = "00"+count;
        } else if(count < 100){
            value = "0"+count;
        } else if (count < 1000){
            value = ""+count;
        }
        
        return value;
    }
    
    public boolean Reset(){
        boolean bool = false;
        String tahun = "";
        
        ResultSet res = counting.GetCounting();
        try {
            if(res.first()){
                tahun = res.getString("jenisKey");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AutoCode.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(!counting.tahun().substring(2, 4).equals(tahun)){
            bool = true;
        }
        
        return bool;
    }
    
    public String Main(){
        String autoCode = "";
        String uniqueKey = this.key;
        
        String tahunKey = counting.tahun().substring(2, 4);
        
        String uniqueCode = UniqueCode(Reset());
        
        autoCode = uniqueKey+tahunKey+uniqueCode;
        return autoCode;
    }
    
    public void Update(String uniqueCode){
        
        String countString = uniqueCode.substring(3,6);
        String keyString = uniqueCode.substring(1,3);
        
//        System.out.println(countString +" "+keyString);
        
        int count = Integer.parseInt(countString);
        int key= Integer.parseInt(keyString);
        counting._Update("jenis_count", count);
        counting._Update("jenisKey", key);
    }
}
