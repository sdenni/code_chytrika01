/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.autocode;

import code.Counting;
import code.localsetting.AutoCode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class TambahBrgAutoCode {
    private static Counting counting = new Counting();
    
    String key = "DT";
    
    public String UniqueCode(boolean reset){
        String value = "001";
        int count = 0;
        
        ResultSet res = counting.GetCounting();
        try {
            if(res.first()){
                if(reset){
                    count = 1;
                } else {
                    count = res.getInt("tambahBrg_count") + 1;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(AutoCode.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(count < 10) {
            value = "00"+count;
        } else if(count < 100){
            value = "0"+count;
        } else if (count < 1000){
            value = ""+count;
        } 
//        else if (count < 10000){
//            value = ""+count;
//        }
        
        return value;
    }
    
    public boolean Reset(){
        boolean bool = false;
        String keyBarang = "";
        
        ResultSet res = counting.GetCounting();
        
        try {
            if(res.first()){
                keyBarang = res.getString("tambahBrgKey");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AutoCode.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(!SubMain().equals(keyBarang)){
            bool = true;
        }
        
        return bool;
    }
    
    public String SubMain(){
        String value = "";
        String tahunKey = counting.tahun().substring(2, 4);
        String bulanKey = counting.bulan();
        String hariKey = counting.hari();
        value = hariKey+bulanKey+tahunKey;
        return value;
    }
    
    public static String Main(){
        TambahBrgAutoCode tbac = new TambahBrgAutoCode();
        String autoCode = "";
        String uniqueKey = tbac.key;
        String uniqueCode = tbac.UniqueCode(tbac.Reset());
        autoCode = uniqueKey+tbac.SubMain()+uniqueCode;
        return autoCode;
    }
    
    public static void Update(String uniqueCode){
        
        String countString = uniqueCode.substring(8,11);
        String keyString = uniqueCode.substring(2,8);
        
//        System.out.println(countString +" "+keyString);
        
        int count = Integer.parseInt(countString);
        String key= keyString;
        counting._Update("tambahBrg_count", count);
        counting._Update("tambahBrgKey", key);
    }
    
//    public static void main(String[] args) {
//        TambahBrgAutoCode bac = new TambahBrgAutoCode();
//        System.out.println(bac.Reset());
//        System.out.println(bac.Main());
//        bac.Update(bac.Main());
//    }
}
