/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.autocode;

import code.Counting;
import code.databases.Sell;
import code.localsetting.AutoCode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class SellBrgAutoCode {
    private static Counting counting = new Counting();
    
    String key = "DP";
    
    public String UniqueCode(boolean reset){
        String value = "001";
        int count = 0;
        
        ResultSet res = counting.GetCounting();
        try {
            if(res.first()){
                if(reset){
                    count = 1;
                } else {
                    count = res.getInt("sellBrg_count") + 1;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(AutoCode.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(count < 10) {
            value = "00"+count;
        } else if(count < 100){
            value = "0"+count;
        } else if (count < 1000){
            value = ""+count;
        } 
//        else if (count < 10000){
//            value = ""+count;
//        }
        
        return value;
    }
    
    public boolean Reset(String mainId){
        boolean bool = false;
        String keyBarang = "";
        
        ResultSet res = counting.GetCounting();
        
        try {
            if(res.first()){
                keyBarang = res.getString("sellBrgKey");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AutoCode.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(!SubMain(mainId).equals(keyBarang)){
            bool = true;
        }
        
        return bool;
    }
    
    public String SubMain(String mainId){
        String value = "";
        String tahunKey = counting.tahun().substring(2, 4);
        String bulanKey = counting.bulan();
        String hariKey = counting.hari();
        value = hariKey+bulanKey+tahunKey+getUniqueId(mainId);
        return value;
    }
    
    public static String getUniqueId(String mainId){
        return mainId.substring(7, 10);
    }
    
    public static String Main(String mainId){
        SellBrgAutoCode tbac = new SellBrgAutoCode();
        String autoCode = "";
        String uniqueKey = tbac.key;
        String uniqueCode = tbac.UniqueCode(tbac.Reset(mainId));
        autoCode = uniqueKey+ tbac.SubMain(mainId) + uniqueCode;
        return autoCode;
    }
    
    public static void Update(String uniqueCode){
        
        String countString = uniqueCode.substring(11,14);
        String keyString = uniqueCode.substring(2,11);
        
        System.out.println(countString +" "+keyString);
        
        int count = Integer.parseInt(countString);
        String key= keyString;
        counting._Update("sellBrg_count", count);
        counting._Update("sellBrgKey", key);
    }
    
    public static void main(String[] args) {
        SellBrgAutoCode bac = new SellBrgAutoCode();
        System.out.println(bac.Reset(Sell.uniqueCode()));
        System.out.println(bac.Main(Sell.uniqueCode()));
        System.out.println(bac.getUniqueId(Sell.uniqueCode()));
        bac.Update(bac.Main(Sell.uniqueCode()));
    }
}
