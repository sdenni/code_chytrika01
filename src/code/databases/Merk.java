/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.databases;

import code.Master;
import code.autocode.MerkAutoCode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class Merk implements Master{

    private Koneksidb koneksi = new Koneksidb();
    String idMerk, merk, keterangan;
    private MerkAutoCode autoCode = new MerkAutoCode();

    public String getIdMerk() {
        return idMerk;
    }

    public void setIdMerk(String idMerk) {
        this.idMerk = idMerk;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
    
    @Override
    public void Create() {
        try {
            String query = "INSERT INTO merks (idMerk, merk, keterangan) "
                    + "VALUES (?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.idMerk);
            prepare.setString(2, this.merk);
            prepare.setString(3, this.keterangan);
            
            koneksi.Insert(prepare);
            
            updateUniqueCode(this.idMerk);
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void Save() {
        try {
            String query = "UPDATE merks SET "
                    + "merk = ?,"
                    + "keterangan = ? "
                    + "WHERE idMerk = ?";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.merk);
            prepare.setString(2, this.keterangan);
            prepare.setString(3, this.idMerk);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void Delete(String id) {
        try {
            String query = "DELETE FROM merks "
                    + "WHERE idMerk = ?";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);

            prepare.setString(1, this.idMerk);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public ResultSet Show() {
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT * FROM merks";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }

    @Override
    public ResultSet Show(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String uniqueCode() {
        return autoCode.Main();
    }
    
    public void updateUniqueCode(String uniqueCode){
        autoCode.Update(uniqueCode);
    }
    
}
