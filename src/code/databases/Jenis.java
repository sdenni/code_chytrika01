/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.databases;

import code.Master;
import code.autocode.JenisAutoCode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class Jenis implements Master {

    private Koneksidb koneksi = new Koneksidb();
    public String idJns, jenis, keterangan;
    private JenisAutoCode autoCode = new JenisAutoCode();

    public String getIdJns() {
        return idJns;
    }

    public void setIdJns(String idJns) {
        this.idJns = idJns;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
    
    
    
    @Override
    public void Create() {
        try {
            String query = "INSERT INTO jenixs (idJns, jenis, keterangan) "
                    + "VALUES (?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.idJns);
            prepare.setString(2, this.jenis);
            prepare.setString(3, this.keterangan);
            
            koneksi.Insert(prepare);
            
            updateUniqueCode(idJns);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void Save() {
        try {
            String query = "UPDATE jenixs SET "
                    + "jenis = ?,"
                    + "keterangan = ? "
                    + "WHERE idJns = ?";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.jenis);
            prepare.setString(2, this.keterangan);
            prepare.setString(3, this.idJns);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void Delete(String id) {
        try {
            String query = "DELETE FROM jenixs "
                    + "WHERE idJns = ?";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);

            prepare.setString(1, this.idJns);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public ResultSet Show() {
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT * FROM jenixs";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }

    @Override
    public ResultSet Show(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String uniqueCode() {
        return autoCode.Main();
    }
    
    public void updateUniqueCode(String uniqueCode){
        autoCode.Update(uniqueCode);
    }
}
