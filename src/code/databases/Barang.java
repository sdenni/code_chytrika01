/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.databases;

import code.Master;
import code.autocode.BarangAutoCode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class Barang implements Master{

    private Koneksidb koneksi = new Koneksidb();
    
    public String idBrg, namaBrg, satuan, idJenis, idMerk, batasan, keterangan;
    public int harga, stok, stokRetur;
    private BarangAutoCode autoCode = new BarangAutoCode();

    public int getStokRetur() {
        return stokRetur;
    }

    public void setStokRetur(int stokRetur) {
        this.stokRetur = stokRetur;
    }
    
    public int getStok() {
        return stok;
    }

    public void setStok(int stok) {
        this.stok = stok;
    }

    public String getIdBrg() {
        return idBrg;
    }

    public void setIdBrg(String idBrg) {
        this.idBrg = idBrg;
    }

    public String getNamaBrg() {
        return namaBrg;
    }

    public void setNamaBrg(String namaBrg) {
        this.namaBrg = namaBrg;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public String getIdJenis() {
        return idJenis;
    }

    public void setIdJenis(String idJenis) {
        this.idJenis = idJenis;
    }

    public String getIdMerk() {
        return idMerk;
    }

    public void setIdMerk(String idMerk) {
        this.idMerk = idMerk;
    }

    public String getBatasan() {
        return batasan;
    }

    public void setBatasan(String batasan) {
        this.batasan = batasan;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
    
    @Override
    public void Create() {
        try {
            
            String query = "INSERT INTO barangs (idBrg, namaBrg, satuan, idJenis, idMerk, batasan, harga, stok, stokRetur, keterangan) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.idBrg);
            prepare.setString(2, this.namaBrg);
            prepare.setString(3, this.satuan);
            prepare.setString(4, this.idJenis);
            prepare.setString(5, this.idMerk);
            prepare.setString(6, this.batasan);
            prepare.setInt(7, this.harga);
            prepare.setInt(8, this.stok);
            prepare.setInt(9, this.stokRetur);
            prepare.setString(10, this.keterangan);
            
            koneksi.Insert(prepare);
            
            updateUniqueCode(this.idBrg);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void Save() {
        try {
            
            String query = "UPDATE barangs SET "
                    + "namaBrg = ? , "
                    + "satuan = ? , "
                    + "idJenis = ? , "
                    + "idMerk = ? ,"
                    + "batasan = ? ,"
                    + "harga = ? ,"
                    + "stok = ? ,"
                    + "keterangan = ? "
                    + "WHERE idBrg = ?";
            
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(9, this.idBrg);
            prepare.setString(1, this.namaBrg);
            prepare.setString(2, this.satuan);
            prepare.setString(3, this.idJenis);
            prepare.setString(4, this.idMerk);
            prepare.setString(5, this.batasan);
            prepare.setInt(6, this.harga);
            prepare.setInt(7, this.stok);
            prepare.setString(8, this.keterangan);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void Delete(String id) {
        try {
            String query = "DELETE FROM barangs "
                    + "WHERE idBrg = ?";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);

            prepare.setString(1, this.idBrg);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public ResultSet Show() {
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT * FROM barangs";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }
    
    public ResultSet ShowGeneralData() {
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT barangs.idBrg, barangs.namaBrg, barangs.satuan, jenixs.jenis, merks.merk, batasan.namaBatasan, barangs.harga, barangs.stok, barangs.stokRetur, barangs.keterangan, barangs.idJenis, barangs.idMerk, barangs.batasan FROM barangs INNER JOIN merks ON barangs.idMerk = merks.idMerk INNER JOIN jenixs ON barangs.idJenis = jenixs.idJns INNER JOIN batasan ON barangs.batasan = batasan.idBatasan";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }
    
    public ResultSet ShowMainInfo(){
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT "
                + "barangs.idBrg as 'kode', "
                + "barangs.namaBrg as 'nama', "
                + "barangs.satuan as 'satuan', "
                + "merks.merk as 'merk', "
                + "jenixs.jenis as 'jenis', "
                + "barangs.harga as 'harga', "
                + "barangs.stok as 'stok', "
                + "barangs.stokRetur as 'retur' "
                + "FROM barangs "
                + "INNER JOIN merks ON barangs.idMerk = merks.idMerk "
                + "INNER JOIN jenixs ON barangs.idJenis = jenixs.idJns";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }
    
    public ResultSet ShowMainInfo(String idBarang){
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT "
                + "barangs.idBrg as 'kode', "
                + "barangs.namaBrg as 'nama', "
                + "barangs.satuan as 'satuan', "
                + "merks.merk as 'merk', "
                + "jenixs.jenis as 'jenis', "
                + "barangs.harga as 'harga', "
                + "barangs.stok as 'stok', "
                + "barangs.stokRetur as 'retur' "
                + "FROM barangs "
                + "INNER JOIN merks ON barangs.idMerk = merks.idMerk "
                + "INNER JOIN jenixs ON barangs.idJenis = jenixs.idJns "
                + "WHERE barangs.idBrg = '"+idBarang+"'";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }

    @Override
    public ResultSet Show(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public int GetJmlBrg(String id){
        int jml = 0;
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT stok FROM barangs "
                + "WHERE idBrg = '"+id+"'";
        
//System.out.println(sqlquery);

        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);
            
            if(rs.first()){
                jml = rs.getInt(1);
            } else {
                jml = 0;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return jml;
    }
    
    public int GetJmlBrgRetur(String id){
        int jml = 0;
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT stokRetur FROM barangs "
                + "WHERE idBrg = '"+id+"'";
        
//System.out.println(sqlquery);

        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);
            
            if(rs.first()){
                jml = rs.getInt(1);
            } else {
                jml = 0;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return jml;
    }
    
    public void UpdateStockBarang(){
        try {
            
            String query = "UPDATE barangs SET "
                    + "stok = ? "
                    + "WHERE idBrg = ?";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(2, this.idBrg);
            prepare.setInt(1, this.stok);
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void UpdateStockBarangRetur(){
        try {
            
            String query = "UPDATE barangs SET "
                    + "stokRetur = ? "
                    + "WHERE idBrg = ?";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(2, this.idBrg);
            prepare.setInt(1, this.stok);
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static int getMelebihiBatas(){
        int value = 0;
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT COUNT(barangs.idBrg) FROM barangs INNER JOIN batasan ON barangs.batasan = batasan.idBatasan WHERE barangs.stok > batasan.max";
        
//System.out.println(sqlquery);

        try {
            st = Koneksidb.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);
            
            if(rs.first()){
                value = rs.getInt(1);
            } else {
                value = 0;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return value;
    }
    
    public static int getKurangBatas(){
        int value = 0;
        
        //SELECT barangs.idBrg as 'kodebarang', barangs.stok as 'Jml', batasan.min as 'Min', batasan.max as 'Max' FROM barangs INNER JOIN batasan ON barangs.batasan = batasan.idBatasan WHERE barangs.stok < batasan.min
        
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT COUNT(barangs.idBrg) FROM barangs INNER JOIN batasan ON barangs.batasan = batasan.idBatasan WHERE barangs.stok < batasan.min";
        
//System.out.println(sqlquery);

        try {
            st = Koneksidb.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);
            
            if(rs.first()){
                value = rs.getInt(1);
            } else {
                value = 0;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return value;
    }
    
    public static String getNamaBrg(String idBrg){
        String value = "";
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT namaBrg FROM barangs WHERE idBrg = '"+idBrg+"'";
        
        try {
            st = Koneksidb.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);
            if (rs.first()) {
                value = rs.getString(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return value;
    }
    
    public String uniqueCode() {
        return autoCode.Main();
    }
    
    public void updateUniqueCode(String uniqueCode){
        autoCode.Update(uniqueCode);
    }
}