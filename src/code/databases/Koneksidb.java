/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.databases;

import code.localsetting.DatabaseSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author S_Denni
 */
public class Koneksidb {
    private static Connection koneksi;
    private static final DatabaseSet info = new DatabaseSet();
//    private static Statement statemen;
//    private static Koneksidb koneksidb;
    
    public static Connection Koneksidb() {
        if (koneksi == null){
            try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Driver berhasil diload");
                try {
                    String path = info.databasepath;
                    String databasename = info.databasename;
                    String username = info.databaseuser;
                    String password = info.databasepass;
                    koneksi = DriverManager.getConnection(path+databasename,username,password);

                    System.out.println("Koneksi Database Sukses");
                    
                } catch (SQLException ex) {
                    System.out.println("Koneksi Gagal ");
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Periksa kembali \nXAMPP, atau nama Database", "Database Error", JOptionPane.WARNING_MESSAGE);
                }
            } catch (ClassNotFoundException ex) {
                System.out.println("class Driver tidak ditemukan");
                System.exit(0);
            } catch (InstantiationException ex){
                
            } catch (IllegalAccessException ex){
                
            }
        }
        return koneksi;
    }
    
    public static void Insert(PreparedStatement prepareStat){
        try {
            prepareStat.execute();          
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Menyimpan ERROR\n"+e+"", "Informasi Create", JOptionPane.WARNING_MESSAGE);
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        Koneksidb();
    }
}
