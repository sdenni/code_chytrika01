/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.databases;

import code.Master;
import code.autocode.SellAutoCode;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class Sell implements Master {

    private Koneksidb koneksi = new Koneksidb();
    
    private String idTrxJual, user, keterangan;
    private java.sql.Date tanggal;
    
    private String idTrxJual_brg, idBrg;
    private int jml;
    
    private static SellAutoCode autoCode = new SellAutoCode();

    public String getIdTrxJual() {
        return idTrxJual;
    }

    public void setIdTrxJual(String idTrxJual) {
        this.idTrxJual = idTrxJual;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getIdTrxJual_brg() {
        return idTrxJual_brg;
    }

    public void setIdTrxJual_brg(String idTrxJual_brg) {
        this.idTrxJual_brg = idTrxJual_brg;
    }

    public String getIdBrg() {
        return idBrg;
    }

    public void setIdBrg(String idBrg) {
        this.idBrg = idBrg;
    }

    public int getJml() {
        return jml;
    }

    public void setJml(int jml) {
        this.jml = jml;
    }
    
    public void InsertIntoJual() {
        try {
            String query = "INSERT INTO trxjual (idTrxJual, user, tanggal, keterangan) "
                    + "VALUES (?,?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.idTrxJual);
            prepare.setString(2, this.user);
            prepare.setDate(3, this.tanggal);
            prepare.setString(4, this.keterangan);
            
            koneksi.Insert(prepare);
            
            updateUniqueCode(this.idTrxJual);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void InsertIntoJualBrg(){
        try {
            String query = "INSERT INTO trxjual_brg (idTrxJual_brg, idTrxJual, idBrg, jml) "
                    + "VALUES (?,?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.idTrxJual_brg);
            prepare.setString(2, this.idTrxJual);
            prepare.setString(3, this.idBrg);
            prepare.setInt(4, this.jml);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void Create() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet Show() {
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT * FROM trxjual";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }
    
    @Override
    public ResultSet Show(String id) {
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT * FROM trxjual";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }
    
    public ResultSet ShowDetSell(String idJual) {
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT * FROM trxjual_brg WHERE trxjual_brg.idTrxJual = '"+idJual+"'";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }

    public ResultSet Show(java.util.Date date1, java.util.Date date2) {
        
        java.sql.Date tanggal1 = new java.sql.Date(date1.getTime());
        java.sql.Date tanggal2 = new java.sql.Date(date2.getTime());
        
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT * FROM trxjual WHERE trxjual.tanggal >= '"+tanggal1+"' AND trxjual.tanggal <= '"+tanggal2+"'";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }
    
    public ResultSet ShowBestJual(java.util.Date date1, java.util.Date date2, int limit) {
        
        java.sql.Date tanggal1 = new java.sql.Date(date1.getTime());
        java.sql.Date tanggal2 = new java.sql.Date(date2.getTime());
        
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT "
                + "barangs.namaBrg as 'barang', "
                + "trxjual_brg.jml as 'terjual' "
                + "FROM trxjual_brg "
                + "INNER JOIN barangs ON trxjual_brg.idBrg = barangs.idBrg "
                + "WHERE trxjual_brg.idTrxJual = "
                + "ANY("
                + "SELECT trxjual.idTrxJual "
                + "FROM trxjual "
                + "WHERE trxjual.tanggal >= '"+tanggal1+"' "
                + "AND trxjual.tanggal <= '"+tanggal2+"' ) "
                + "GROUP BY barangs.idBrg ORDER BY trxjual_brg.jml DESC "
                + "LIMIT "+limit+"";
        
//        System.err.println("QUERY "+sqlquery);
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }
    
    public ResultSet ShowLowestJual(java.util.Date date1, java.util.Date date2, int limit) {
        
        java.sql.Date tanggal1 = new java.sql.Date(date1.getTime());
        java.sql.Date tanggal2 = new java.sql.Date(date2.getTime());
        
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT "
                + "barangs.namaBrg as 'barang', "
                + "trxjual_brg.jml as 'terjual' "
                + "FROM trxjual_brg "
                + "INNER JOIN barangs ON trxjual_brg.idBrg = barangs.idBrg "
                + "WHERE trxjual_brg.idTrxJual = "
                + "ANY("
                + "SELECT trxjual.idTrxJual "
                + "FROM trxjual "
                + "WHERE trxjual.tanggal >= '"+tanggal1+"' "
                + "AND trxjual.tanggal <= '"+tanggal2+"' ) "
                + "GROUP BY barangs.idBrg ORDER BY trxjual_brg.jml ASC "
                + "LIMIT "+limit+"";
        
//        System.err.println("QUERY "+sqlquery);
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }
    
    public java.util.Date getFirstDate(){
        java.util.Date value = null;
        
        java.sql.Date sementara = null;
        
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT trxjual.tanggal FROM trxjual ORDER BY trxjual.tanggal ASC LIMIT 1";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);
            
            if (rs.first()) {
                sementara = rs.getDate(1);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        value = new Date(sementara.getTime());
        
        return value;
    }
    
    public java.util.Date getLastDate(){
        java.util.Date value = null;
        
        java.sql.Date sementara = null;
        
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT trxjual.tanggal FROM trxjual ORDER BY trxjual.tanggal DESC LIMIT 1";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);
            
            if (rs.first()) {
                sementara = rs.getDate(1);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        value = new Date(sementara.getTime());
        
        return value;
    }
    
    public static String getIdBrg(String idTrxJualBrg){
        String value = "";
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT idBrg FROM trxjual_brg WHERE idTrxJual_brg = '"+idTrxJualBrg+"'";
        
        try {
            st = Koneksidb.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);
            if (rs.first()) {
                value = rs.getString(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return value;
    }
    
    
    
    public static String uniqueCode() {
        return autoCode.Main();
    }
    
    public void updateUniqueCode(String uniqueCode){
        autoCode.Update(uniqueCode);
    }
    
}
