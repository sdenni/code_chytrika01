/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.databases;

import code.Master;
import code.autocode.PenggunaAutoCode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class User implements Master {

    private Koneksidb koneksi = new Koneksidb();
    private String idUser, nama, username, password;
    private int auth;
    private PenggunaAutoCode autoCode = new PenggunaAutoCode();

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAuth() {
        return auth;
    }

    public void setAuth(int auth) {
        this.auth = auth;
    }
    
    
    
    
    @Override
    public void Create() {
        try {
            String query = "INSERT INTO users (idUsr, nama, username, password, auth) "
                    + "VALUES (?,?,?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.idUser);
            prepare.setString(2, this.nama);
            prepare.setString(3, this.username);
            prepare.setString(4, this.password);
            prepare.setInt(5, this.auth);
            
            koneksi.Insert(prepare);
            
            updateUniqueCode(idUser);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void Save() {
        try {
            String query = "UPDATE users SET "
//                    + "username = ?,"
//                    + "password = ?, "
                    + "auth = ? "
                    + "WHERE idUsr = ?";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
//            prepare.setString(1, this.username);
//            prepare.setString(2, this.password);
            prepare.setInt(1, this.auth);
            prepare.setString(2, this.idUser);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void Delete(String id) {
        try {
            String query = "DELETE FROM users "
                    + "WHERE idUsr = ?";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);

            prepare.setString(1, this.idUser);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public ResultSet Show() {
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT idUsr AS 'Kode', nama AS 'Nama', username AS 'Username', "
                + "(CASE "
                + "WHEN auth = 1 THEN 'Pemilik' "
                + "WHEN auth = 2 THEN 'Pegawai' "
                + "ELSE 'something wrong' "
                + "END) AS 'Hak Akses'"
                + "FROM users";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }

    @Override
    public ResultSet Show(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ResultSet ShowByUsername(String username){
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT username, password FROM users";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }
    
    public static String getNamaUser(String namaUser){
        
        String value = "";
//        ResultSet rs = null;
//        Statement st;
//        String sqlquery = "SELECT namaBrg FROM barangs WHERE idBrg = '"+idBrg+"'";
//        
//        try {
//            st = koneksi.Koneksidb().createStatement();
//            rs = st.executeQuery(sqlquery);
//            if (rs.first()) {
//                value = rs.getString(1);
                value = "nama "+namaUser;
                
//            }
//        } catch (SQLException e) {
//            System.out.println(e);
//        }
//        
        return value;
    }
    
    public boolean ValidateLogin(String Username, String Password){
        boolean bool = false;
        String user = null;
        String pass = null;
        
        try {
            ResultSet result = this.ShowByUsername(Username);
            if (result.next()) {
                user = result.getString(1);
                pass = result.getString(2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(user.equals(Username)) {
            if (pass.equals(Password)) {
                bool = true;
            }
        }
        return bool;
    }
    
    public String uniqueCode() {
        return autoCode.Main();
    }
    
    public void updateUniqueCode(String uniqueCode){
        autoCode.Update(uniqueCode);
    }
    
}
