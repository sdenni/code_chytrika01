/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.databases;

import code.Master;
import code.autocode.BatasanAutoCode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class Batasan implements Master{

    private Koneksidb koneksi = new Koneksidb();
    String idBatasan, namaBatasan;
    int min,max;
    private BatasanAutoCode autoCode = new BatasanAutoCode();

    public String getIdBatasan() {
        return idBatasan;
    }

    public void setIdBatasan(String idBatasan) {
        this.idBatasan = idBatasan;
    }

    public String getNamaBatasan() {
        return namaBatasan;
    }

    public void setNamaBatasan(String namaBatasan) {
        this.namaBatasan = namaBatasan;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
    
    @Override
    public void Create() {
        try {
            String query = "INSERT INTO batasan (idBatasan, namaBatasan, min, max) "
                    + "VALUES (?,?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.idBatasan);
            prepare.setString(2, this.namaBatasan);
            prepare.setInt(3, this.min);
            prepare.setInt(4, this.max);
            
            koneksi.Insert(prepare);
            
            updateUniqueCode(this.idBatasan);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void Save() {
        try {
            String query = "UPDATE batasan SET "
                    + "namaBatasan = ?, "
                    + "min = ?, "
                    + "max = ? "
                    + "WHERE idBatasan = ?";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);

            prepare.setString(1, this.namaBatasan);
            prepare.setInt(2, this.min);
            prepare.setInt(3, this.max);
            prepare.setString(4, this.idBatasan);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void Delete(String id) {
        try {
            String query = "DELETE FROM batasan "
                    + "WHERE idBatasan = ?";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);

            prepare.setString(1, this.idBatasan);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public ResultSet Show() {
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT * FROM batasan";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }

    @Override
    public ResultSet Show(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String uniqueCode() {
        return autoCode.Main();
    }
    
    public void updateUniqueCode(String uniqueCode){
        autoCode.Update(uniqueCode);
    }
}
