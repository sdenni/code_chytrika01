/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.databases;

import code.Master;
import code.autocode.ReturAutoCode;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class Retur implements Master {

    private Koneksidb koneksi = new Koneksidb();  
    private static ReturAutoCode autoCode = new ReturAutoCode();
    
    String idTrxRetur, user, keterangan;
    java.sql.Date tanggal;
    
    String idTrxRetur_brg, idTrxJual_brg;
    int jumlah;

    public String getIdTrxRetur() {
        return idTrxRetur;
    }

    public void setIdTrxRetur(String idTrxRetur) {
        this.idTrxRetur = idTrxRetur;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getIdTrxRetur_brg() {
        return idTrxRetur_brg;
    }

    public void setIdTrxRetur_brg(String idTrxRetur_brg) {
        this.idTrxRetur_brg = idTrxRetur_brg;
    }

    public String getIdTrxJual_brg() {
        return idTrxJual_brg;
    }

    public void setIdTrxJual_brg(String idTrxJual_brg) {
        this.idTrxJual_brg = idTrxJual_brg;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }
    
    public void InsertIntoRetur() {
        try {
            String query = "INSERT INTO trxretur (idTrxRtr, user, tanggal, keterangan) "
                    + "VALUES (?,?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.idTrxRetur);
            prepare.setString(2, this.user);
            prepare.setDate(3, this.tanggal);
            prepare.setString(4, this.keterangan);
            
            koneksi.Insert(prepare);
            
            updateUniqueCode(this.idTrxRetur);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void InsertIntoReturBrg(){
        try {
            String query = "INSERT INTO trxretur_brg (idTrxRetur_brg, idTrxRtr, idTrxJual_brg, jml, keterangan) "
                    + "VALUES (?,?,?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.idTrxRetur_brg);
            prepare.setString(2, this.idTrxRetur);
            prepare.setString(3, this.idTrxJual_brg);
            prepare.setInt(4, this.jumlah);
            prepare.setString(5, this.keterangan);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void Create() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet Show() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet Show(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public static String uniqueCode() {
        return autoCode.Main();
    }
    
    public void updateUniqueCode(String uniqueCode){
        autoCode.Update(uniqueCode);
    }
}
