/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.databases;

import code.Master;
import code.autocode.TambahAutoCode;
import code.autocode.TambahBrgAutoCode;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class TambahBrg implements Master {

    private Koneksidb koneksi = new Koneksidb();
    private String idTambah,user,keterangan;
    private java.sql.Date tanggal;
    private String idTrxTambah_brg, idBrg;
    private int jml;
    private static TambahAutoCode autoCode = new TambahAutoCode();

    public String getIdTrxTambah_brg() {
        return idTrxTambah_brg;
    }

    public void setIdTrxTambah_brg(String idTrxTambah_brg) {
        this.idTrxTambah_brg = idTrxTambah_brg;
    }

    public String getIdBrg() {
        return idBrg;
    }

    public void setIdBrg(String idBrg) {
        this.idBrg = idBrg;
    }

    public int getJml() {
        return jml;
    }

    public void setJml(int jml) {
        this.jml = jml;
    }

    public String getIdTambah() {
        return idTambah;
    }

    public void setIdTambah(String idTambah) {
        this.idTambah = idTambah;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }
    
    public void InsertIntoTambah() {
        try {
            String query = "INSERT INTO trxtambah (idTambah, user, tanggal, keterangan) "
                    + "VALUES (?,?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.idTambah);
            prepare.setString(2, this.user);
            prepare.setDate(3, this.tanggal);
            prepare.setString(4, this.keterangan);
            
            koneksi.Insert(prepare);
            
            updateUniqueCode(this.idTambah);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void InsertIntoTambahBrg(){
        try {
            String query = "INSERT INTO trxtambah_brg (idTrxTambah_brg, idTambah, idBrg, jml) "
                    + "VALUES (?,?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.idTrxTambah_brg);
            prepare.setString(2, this.idTambah);
            prepare.setString(3, this.idBrg);
            prepare.setInt(4, this.jml);
            
            koneksi.Insert(prepare);
            TambahBrgAutoCode.Update(this.idTrxTambah_brg);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void Create() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet Show() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet Show(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public static String uniqueCode() {
        return autoCode.Main();
    }
    
    public void updateUniqueCode(String uniqueCode){
        autoCode.Update(uniqueCode);
    }
    
}
