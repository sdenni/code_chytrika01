/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

import code.databases.Barang;

/**
 *
 * @author S_Denni
 */
public class Calculate {
    
    Barang brg = new Barang();
    
    public int CalTambahBarang(String idBrg, int value){
        int lastSum = brg.GetJmlBrg(idBrg);
        int sum = lastSum + value;
        return sum;
    }
    
    public int CalKurangiBarang(String idBrg, int value){
        int lastSum = brg.GetJmlBrg(idBrg);
        int sum = lastSum - value;
        return sum;
    }
    
    public int CalTambahBarangRetur(String idBrg, int value){
//        System.out.println("MENJALANKAN TAMBAH RETUR");
        int lastSum = brg.GetJmlBrgRetur(idBrg);
        int sum = lastSum + value;
//        System.out.println("Akhir TAMBAH RETUR sum= "+ sum);
        return sum;
        
    }
    
    public int CalKurangiBarangRetur(String idBrg, int value){
        int lastSum = brg.GetJmlBrgRetur(idBrg);
        int sum = lastSum - value;
        return sum;
    }
}
